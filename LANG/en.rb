SERVER_DOC =<<"EOF"
Usage:
  ruby server.rb [arguments]

Option:
  -h --help         show this message
  -f <file name>    configuration file (default: server.yml)
  -e                removes the keys contained in the database
EOF

CLIENT_DOC =<<"EOF"
Usage:
  ruby client.rb -a <key id> [arguments]
  -a <key id>       positive integer identifier key
  -n or -g          one of the two parameters should be
  
Option:
  -h --help         show this message
  -n                create a new key
  -g                show key
  -s <file name>    path to the file shared secret (default: secret.dat)
  -i                IP server (default: 127.0.0.1)
  -p                port server (default: 3000)
  
EOF

FILE_NOT_FOUND = "error: file not found"
ERROR_CONNECTION = "error: connecting to server"
EXPECT_CONNECTION_REDIS = "expect server connection Redis"
LISTENING_ON = "listening on"
REQUEST = "request: "
NEW_KEY_MESSAGE = "new key generated:"
KEY_RECEIVED = "key received"
ERROR_KEY_RECEIVED = "error: key does not exists"
ERROR_NEW_KEY_RECEIVED ="error: key already exists"
ERROR_FORBIDEN = "error: not authorized"
