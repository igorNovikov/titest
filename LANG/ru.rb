SERVER_DOC =<<"EOF"
Usage:
  ruby server.rb [arguments]

Option:
  -h, --help        показать это сообщение
  -f <file name>    файл конфигурации (по-умолчанию: server.yml)
  -e                сервер удаляет содержащиеся в БД ключи
EOF

CLIENT_DOC =<<"EOF"
Usage:
  ruby client.rb –a <key id> [arguments]
  -a <key id>       целочисленный положительный идентификатор ключа
  -n or -g          один из двух параметров должен быть
  
Option:
  -h –-help         показать это сообщение
  -n                создать новый ключ
  -g                получить ключ
  -s <file name>    путь к файлу shared secret (по-умолчанию: secret.dat)
  -i                IP сервера (по-умолчанию: 127.0.0.1)
  -p                порт сервера (по-умолчанию: 3000)
  
EOF

FILE_NOT_FOUND = "не найден файл"
ERROR_CONNECTION = "ошибка подключения к серверу"
EXPECT_CONNECTION_REDIS = "ожидаем подключение сервера Redis"
LISTENING_ON = "используется"
REQUEST = "запрос: "
NEW_KEY_MESSAGE = "новый ключ сгенерирован:"
KEY_RECEIVED = "ваш ключ"
ERROR_KEY_RECEIVED = "ошибка: ключ не существует"
ERROR_NEW_KEY_RECEIVED = "ошибка: ключ уже выдан"
ERROR_FORBIDEN = "ошибка: нет доступа"

