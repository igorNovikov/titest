# Испытательное задание для разработчика Инвентос #

- https://bitbucket.org/igorNovikov/titest

	выполнил: [Игорь Новиков][Igor Novikov]

## DESCRIPTION: ##

Реализовать две программы: сервер и клиент для хранения и обмена секретными ключами шифрования.

## Version ##
0.0.3

## FEATURES ##

* *Ruby 1.9.3 and 2.2.3*
* *Redis 2.8.2101*
* *Both Window/Linux supported*
* *Includes gems based on usage, or from a Bundler Gemfile*

## BUILDING && INSTALLATION: ##

### Установка репозитория: ###

```sh
> git clone https://bitbucket.org/igorNovikov/titest.git
```

### Сборка под Ubuntu: ###

для Ruby 2.2.3

```sh
cd titest
make
```

для Ruby 1.9.3

```sh
cd titest
make build_193
```

### Сборка под Window: ###

```sh
cd titest
bundle install
```

Сборка клиента для Ruby 2.2.3

```sh
> rake 
```

или

```sh
> rake buildclient
```

Сборка сервера для Ruby 2.2.3

```sh
> rake buildserver
```

Сборка клиента для Ruby 1.9.3

```sh
> rake buildclient_193
```

Сборка сервера для Ruby 1.9.3

```sh
> rake buildserver_193
```

### Создание файла secret.dat: ###

```sh
> ruby -e "require 'securerandom'; print SecureRandom.hex(32)" > \secret.dat
```

## SYNOPSIS: ##

### Клиент (client) ###

client — консольное приложение, которое может получать секретный ключ c сервера по уникальному ID ключа.

```sh
> client -a 123 -n
new key generated: 61440d722a34562db9775d1c1e407c03
> client -a 123 -n
new key generated: 61440d722a34562db9775d1c1e407c03
> client -a 123 -g
key received: 61440d722a34562db9775d1c1e407c03
> client -a 124 -g
error: key does not exists
> client -a 123 -n
error: key already exists
```

параметры:

- a — (обязательный) целочисленный положительный идентификатор ключа
- n — (необязательный) создать новый ключ
- g — (необязательный) получить ключ
- s — (необязательный) путь к файлу shared secret. По-умолчанию: secret.dat
- i — (необязательный) IP сервера. По-умолчанию: 127.0.0.1
- p — (необязательный) порт сервера. По-умолчанию: 3000

### Сервер (server) ###

server — приложение, обслуживающее запросы клиентов и хранящее информацию о ключах в СУБД Redis. Сервер должен сохранять данные при перезапусках.

```sh
> server -f server.conf
listening on 127.0.0.1:3000
```

параметры:

- f — (необязательный) путь к конфигурационному файлу в формате YAML. По-умолчанию: server.yml
- e — (необязательный) при запуске с этим параметром сервер предварительно полностью удаляет содержащиеся в БД ключи.

Используйте следующий образец конфигурационного файла для разработки:

```text
 production:
  host: 127.0.0.1
  port: 3000
  shared_secret_path: secret.dat
  redis_host: 127.0.0.1
  redis_port: 6379
  redis_db: 0
```

### Протокол ###

Клиент и сервер должны обмениваться сообщениями только при совпадении их shared secret файлов.

```sh
> client -a 123 -n -s invalid_secret.dat
error: not authorized
```

По всем вопросам можете обращаться на [igor@sitdb.ru][igoremail] и скайпу [i.novikov78] мой прифиль: [Igor Novikov] по проекту: [titest]


**Благодарю, за потраченное время, Игорь Новиков!**


   [titest]: <https://bitbucket.org/igorNovikov/titest>
   [Igor Novikov]: <https://bitbucket.org/igorNovikov>
   [igoremail]: <mailto:igor@sitdb.ru>
   [i.novikov78]: <i.novikov78>
   [git-repo-url]: <https://bitbucket.org/igorNovikov/titest.git>
