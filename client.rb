#!/usr/bin/env ruby

require './LANG/ru'
require './lib/client'

begin
    s1 = Client.new()
    if s1.if_arg
       s1.read_key.connect.dialog
    end
rescue SystemExit, Interrupt
end	