require 'socket'
require 'json'
require 'digest'


class Client
	
    def initialize
        @data = {}
        @file_key = "secret.dat"
        @ip = "127.0.0.1"
        @port = "3000"
    end	

    def if_arg
        doc = CLIENT_DOC
        if ARGV.empty?
            puts doc
            exit 0
        else
            ARGV.each_with_index do |key, i|
                case key
                    when "-h"
                        puts doc
                        exit 0

                    when "--help"
                        puts doc
                        exit 0

                    when "-a"
                        if ARGV[i+1] && ARGV[i+1].match(/^(\d)+$/)
                            @data["name"] = ARGV[i+1]
                        else 
                            puts doc
                            exit 0
                        end
                    when "-n"
                        @data["action"] = "create"

                    when "-g"
                        @data["action"] = "view"

                    when "-s"
                        if ARGV[i+1]
                            @file_key = ARGV[i+1]
                        end			

                    when "-i"
                        if ARGV[i+1]
                            @ip = ARGV[i+1]
                        end					

                    when "-p"
                        if ARGV[i+1]
                            @port = ARGV[i+1]
                        end	

                    when "-old"
                        #  недокументированная фича
                        #  -old <key data>   ранее полученный ключ, возможно использование только совместно с параметром -n, для обновления ключа без уведомления оператора (так было бы веселее)						
                        if ARGV[i+1]
                            @data["old_key"] = ARGV[i+1]
                        end	

                end
            end
        end
		
        if (@data["action"] == nil) ||
          (@data["old_key"] != nil && @data["action"] != "create")
            puts doc
            exit 0

        end	
        true	
    end
	
    def read_key
        salt = "QxLUF1bgIAdeQX"
        begin
            text = File.open("./" + @file_key){ |file| file.read }.force_encoding('utf-8')
        rescue Errno::ENOENT
            puts "#{FILE_NOT_FOUND} '" + @file_key + "'"
            exit 0
        end
        text = @data["name"] + text + salt
        @data["secret_key"] = Digest::SHA256.base64digest text
        self
    end
	
    def connect
        begin
            @conn = TCPSocket.new(@ip, @port) 
        rescue Errno::ECONNREFUSED
            puts "#{ERROR_CONNECTION} '" + @ip + ":" + @port + "'"
        end	
        self
    end
	
    def	dialog
        if @conn
            @conn.write @data.to_json
            puts @conn.recv(1024).force_encoding('utf-8')
            @conn.close
        end	
    end

end	