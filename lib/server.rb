require 'socket'	
require 'yaml'
require 'json'
require 'redis'
require 'digest'
require 'io/console'

class Server
	
    def initialize
        @file_conf = "server.yml"
        @del_data = false
    end	

    def if_arg
        doc = SERVER_DOC

        ARGV.each_with_index do |key, i|
            case key
                when "-h"
                    puts doc
                    exit 0

                when "--help"
                    puts doc
                    exit 0

                when "-f"
                    if ARGV[i+1]
                            @file_conf = ARGV[i+1]
                    else 
                            puts doc
                            exit 0
                    end

                when "-e"
                    @del_data = true

            end
        end
        true
    end
	
    def read_config
        begin
            config = YAML.load_file("./" + @file_conf)
        rescue Errno::ENOENT
            puts "#{FILE_NOT_FOUND} '" + @file_conf + "'"
            exit 0
        end

        production = config["production"] ? config["production"] : {}
        @host = production["host"] ? production["host"].to_s : "127.0.0.1"
        @port = production["port"] ? production["port"].to_s : "3000"
        @shared_secret_path = production["shared_secret_path"] ? production["shared_secret_path"].to_s : "secret.dat"
        @redis_host = production["redis_host"] ? production["redis_host"].to_s : "127.0.0.1"
        @redis_port = production["redis_port"] ? production["redis_port"].to_s : "6380"
        @redis_db = production["redis_db"] ? production["redis_db"].to_s : "0"
        self
    end
	
    def connected
        @redis = Redis.new(:host => @redis_host, :port => @redis_port, :db => @redis_db)
        begin
            @redis.ping
            if @del_data
                @redis.FLUSHDB
            end
        rescue Redis::CannotConnectError
            puts "#{ERROR_CONNECTION} Redis"
            if @del_data
                exit 0
            else
                puts EXPECT_CONNECTION_REDIS
            end
        end	

        a = TCPServer.new(@host, @port) # '' means to bind to "all interfaces", same as nil or '0.0.0.0'

        puts "#{LISTENING_ON} " + @host + ":" + @port
        loop {
            @connection = a.accept
            dialog
        }
    end
	
    def dialog
        if @connection
            jData = @connection.recv(1024)
            data = JSON.parse(jData)

            puts "#{REQUEST}: " + jData.force_encoding('utf-8')
            if is_verify_key(data["name"], data["secret_key"])
                begin
                    if data["action"] == "view"
                        key = @redis.get(data["name"])
                        result = key ? "#{KEY_RECEIVED}: " + key : ERROR_KEY_RECEIVED
                    end		
                    if data["action"] == "create"
                        key = @redis.get(data["name"])
                        if !key || data["old_key"] == key	
                            key = random_string
                            @redis.set(data["name"], key)
                            result = "#{NEW_KEY_MESSAGE}: " + key
                        else
                            result = ERROR_NEW_KEY_RECEIVED
                        end	
                    end			
                rescue Redis::CannotConnectError
                    puts ERROR_REDIS
                    result = ERROR_FORBIDEN
                end	
            else
                result = ERROR_FORBIDEN
            end			
            @connection.write result
            @connection.close			
        end	
    end
	
    def read_file_key
        begin
            salt = "QxLUF1bgIAdeQX"
            @text_file = File.open("./" + @shared_secret_path){ |file| file.read }.force_encoding('utf-8')
            @text_file += salt
        rescue Errno::ENOENT
            puts "#{FILE_NOT_FOUND} '" + @shared_secret_path + "'"
            exit 0
        end
        self
    end
	
    def is_verify_key(name, secret_key)
        secret_key2 = Digest::SHA256.base64digest name + @text_file
        secret_key == secret_key2 ? true : false
    end
	
    def random_string(length=32)
        chars = '0123456789abcdef'
        password = ''
        length.times { password << chars[rand(chars.size)] }
        password
    end	
	
end	

