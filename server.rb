#!/usr/bin/env ruby

require './LANG/ru'
require './lib/server'

begin
    s1 = Server.new()
    if s1.if_arg
        s1.read_config.read_file_key.connected
    end
rescue SystemExit, Interrupt
end	