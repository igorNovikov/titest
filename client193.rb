#!/usr/bin/env ruby1.9.3

require './LANG/en'
require './lib/client'

begin
    s1 = Client.new()
    if s1.if_arg
       s1.read_key.connect.dialog
    end
rescue SystemExit, Interrupt
end	