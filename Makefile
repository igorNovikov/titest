all: build
depending:
	bundle install
cp_223:
	cp server.rb server
	cp client.rb client
cp_193:
	cp server193.rb server
	cp client193.rb client
ch_mod:
	chmod 744 server
	chmod 744 client
build: build_223
build_223: depending cp_223 ch_mod
build_193: depending cp_193 ch_mod
clean:
	rm server
	rm client
	rm secret.dat
