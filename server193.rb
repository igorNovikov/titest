#!/usr/bin/env ruby1.9.3

require './LANG/en'
require './lib/server'

begin
    s1 = Server.new()
    if s1.if_arg
        s1.read_config.read_file_key.connected
    end
rescue SystemExit, Interrupt
end	